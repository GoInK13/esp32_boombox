//START includes
//Work RF reception
#include <RCSwitch.h>
//Work on amplifier
#include <DFRobot_MAX98357A.h>
//Work with CANBus
#include <ESP32-TWAI-CAN.hpp>
//END includes

//START defines
#define ENABLE 1
#define DISABLE 0
#define DEBUG ENABLE

//Defines pinout :
#define PIN_RF 22
#define PIN_AMP_BCLK GPIO_NUM_25
#define PIN_AMP_LRC GPIO_NUM_26
#define PIN_AMP_DIN GPIO_NUM_27
#define PIN_SD_CS GPIO_NUM_5
#define PIN_SD_MOSI GPIO_NUM_23
#define PIN_SD_MISO GPIO_NUM_19
#define PIN_SD_CLK GPIO_NUM_18
#define CAN_TX		GPIO_NUM_17
#define CAN_RX		GPIO_NUM_16
//END defines

//START Global variables
//Amplifier variables
DFRobot_MAX98357A amplifier;  // instantiate an object to control the amplifier
String musicList[100];        // SD card music list
//RF variables
RCSwitch mySwitch = RCSwitch();

TaskHandle_t TaskSound;
TaskHandle_t TaskOthers;
// Define Queue handle
QueueHandle_t QueueRf;
const int QueueRfElementSize = 10;
typedef struct {
  uint8_t buttonClicked;
} rf_t;


CanFrame rxFrame;
//END Global variables



void sendObdFrame(uint8_t obdId) {
	CanFrame obdFrame = { 0 };
	obdFrame.identifier = 0x7DF; // Default OBD2 address;
	obdFrame.extd = 0;
	obdFrame.data_length_code = 8;
	obdFrame.data[0] = 2;
	obdFrame.data[1] = 1;
	obdFrame.data[2] = obdId;
	obdFrame.data[3] = 0xAA;    // Best to use 0xAA (0b10101010) instead of 0
	obdFrame.data[4] = 0xAA;    // CAN works better this way as it needs
	obdFrame.data[5] = 0xAA;    // to avoid bit-stuffing
	obdFrame.data[6] = 0xAA;
	obdFrame.data[7] = 0xAA;
    // Accepts both pointers and references 
    ESP32Can.writeFrame(obdFrame);  // timeout defaults to 1 ms

  Serial.println("CAN SENT!");
}


void setup() {
  // put your setup code here, to run once:
#if DEBUG == ENABLE
  Serial.begin(115200);
  Serial.println("Starting 2");
#endif
  //Init RF receiver, power with 3V3
  pinMode(PIN_RF, INPUT);
  mySwitch.enableReceive(PIN_RF);  // Receiver on pin RF
	ESP32Can.setPins(CAN_TX, CAN_RX);  
  ESP32Can.setRxQueueSize(5);
	ESP32Can.setTxQueueSize(5);
  // .setSpeed() and .begin() functions require to use TwaiSpeed enum,
  // but you can easily convert it from numerical value using .convertSpeed()
  ESP32Can.setSpeed(ESP32Can.convertSpeed(500));
  // You can also just use .begin()..
  if(ESP32Can.begin()) {
      Serial.println("CAN bus started!");
  } else {
      Serial.println("CAN bus failed!");
  }

  /**
   * @brief Initialize I2S
   * @param _bclk - I2S communication pin number, serial clock (SCK), aka bit clock (BCK)
   * @param _lrclk - I2S communication pin number, word select (WS), i.e. command (channel) select, used to switch between left and right channel data
   * @param _din - I2S communication pin number, serial data signal (SD), used to transmit audio data in two's complement format
   * @return true on success, false on error
   */
  while (!amplifier.initI2S(/*_bclk=*/GPIO_NUM_25, /*_lrclk=*/GPIO_NUM_26, /*_din=*/GPIO_NUM_27)) {
#if DEBUG == ENABLE
    Serial.println("Initialize I2S failed !");
#endif
    delay(3000);
  }
  /**
   * @fn initSDCard
   * @brief Initialize SD card
   * @param csPin cs pin number for spi communication of SD card module
   * @return true on success, false on error
   * @note Pin connection of SD card module
   * @n  SD Card |  ESP32
   * @n    +5V   |   VCC(5V)
   * @n    GND   |   GND
   * @n    SS    |   csPin (default to be IO5, can be customized through init function)
   * @n    SCK   |   SCK(IO18)
   * @n    MISO  |   MISO(IO19)
   * @n    MOSI  |   MOSI(IO23)
   * @n  Search MicroSD card reader module at www.dfrobot.com
   */
  while (!amplifier.initSDCard(/*csPin=*/GPIO_NUM_5)) {
#if DEBUG == ENABLE
    Serial.println("Initialize SD card failed !");
#endif
    delay(3000);
  }
#if DEBUG == ENABLE
  Serial.println("Initialize succeed!");
#endif

  /**
   * @brief Scan the music files in WAV format in the SD card
   * @param musicList - The music files in .wav format scanned from the SD card. Type is character string array.
   * @return None
   * @note Only support English for music file and path name currently and try not to use spaces, only support .wav for the audio format currently
   */
  amplifier.scanSDMusic(musicList);

#if DEBUG == ENABLE
  printMusicList();
#endif
  amplifier.setVolume(5);
  amplifier.closeFilter();
  /*
  amplifier.SDPlayerControl(SD_AMPLIFIER_PLAY);
  amplifier.SDPlayerControl(SD_AMPLIFIER_STOP);
  delay(5000);  // Play for 5 seconds first, clearly separated from the next operation of skipping the song
  */
  xTaskCreate/*PinnedToCore*/(
    TaskOthersFunction, /* Function to implement the task */
    "TaskOthers",       /* Name of the task */
    10000,              /* Stack size in words */
    NULL,               /* Task input parameter */
    6,                  /* Priority of the task */
    &TaskOthers        /* Task handle. */
    //,0
    );                 /* Core where the task should run */

  xTaskCreate/*PinnedToCore*/(
    TaskSoundFunction, /* Function to implement the task */
    "TaskSound",       /* Name of the task */
    10000,             /* Stack size in words */
    NULL,              /* Task input parameter */
    0,                 /* Priority of the task */
    &TaskSound        /* Task handle. */
    //,1
    );                /* Core where the task should run */

  QueueRf = xQueueCreate(QueueRfElementSize, sizeof(rf_t));
}

void loop() {
  //delay(65000);  // While not being used yield the CPU to other tasks
  delay(2000);  // While not being used yield the CPU to other tasks

  sendObdFrame(5); // For coolant temperature
  
  // You can set custom timeout, default is 1000
  if(ESP32Can.readFrame(rxFrame, 1000)) {
      // Comment out if too many frames
      Serial.printf("CAN:%03X#%02hx%02hx%02hx%02hx %02hx%02hx%02hx%02hx \r\n", rxFrame.identifier,
        rxFrame.data[0], 
        rxFrame.data[1], 
        rxFrame.data[2], 
        rxFrame.data[3], 
        rxFrame.data[4], 
        rxFrame.data[5], 
        rxFrame.data[6],
        rxFrame.data[7]
        );

        rf_t message;
        message.buttonClicked = rxFrame.data[0];
        amplifier.SDPlayerControl(SD_AMPLIFIER_STOP);
        // The line needs to be passed as pointer to void.
        // The last parameter states how many milliseconds should wait (keep trying to send) if is not possible to send right away.
        // When the wait parameter is 0 it will not wait and if the send is not possible the function will return errQUEUE_FULL
        int ret = xQueueSend(QueueRf, (void*)&message, 0);
  }
  //vTaskDelay(1000);
}

void TaskOthersFunction(void* parameter) {
  for (;;) {
    // put your main code here, to run repeatedly:
    static long timestamp = 0;
    if (millis() - timestamp > 500) {
      timestamp = millis();
      // Do work every 0.5s
      parseSerialCommand();
    }

    //Check receiving RF
    if (mySwitch.available()) {
#if DEBUG == ENABLE
      Serial.print("Received ");
      Serial.print(mySwitch.getReceivedValue());
      Serial.print(" / ");
      Serial.print(mySwitch.getReceivedBitlength());
      Serial.print("bit ");
      Serial.print("Protocol: ");
      Serial.println(mySwitch.getReceivedProtocol());
#endif

      if (mySwitch.getReceivedProtocol() == 11 && mySwitch.getReceivedBitlength() == 18) {
//        amplifier.SDPlayerControl(SD_AMPLIFIER_STOP);
        if (QueueRf != NULL && uxQueueSpacesAvailable(QueueRf) > 0) {
          Serial.println("Click");
          rf_t message;
          message.buttonClicked = mySwitch.getReceivedValue();
          amplifier.SDPlayerControl(SD_AMPLIFIER_STOP);
          // The line needs to be passed as pointer to void.
          // The last parameter states how many milliseconds should wait (keep trying to send) if is not possible to send right away.
          // When the wait parameter is 0 it will not wait and if the send is not possible the function will return errQUEUE_FULL
          int ret = xQueueSend(QueueRf, (void*)&message, 0);
          Serial.println("Sent");
          if (ret == pdTRUE) {
            // The message was successfully sent.
          } else if (ret == errQUEUE_FULL) {
            // Since we are checking uxQueueSpacesAvailable this should not occur, however if more than one task should
            //   write into the same queue it can fill-up between the test and actual send attempt
            Serial.println("The `TaskReadFromSerial` was unable to send data into the Queue");
          }  // Queue send check
        }
      }
      mySwitch.resetAvailable();
    }
    vTaskDelay(10);
  }
}

void TaskSoundFunction(void* parameter) {
  for (;;) {
    if (QueueRf != NULL) {  // Sanity check just to make sure the queue actually exists
      rf_t message;
      int ret = xQueueReceive(QueueRf, &message, portMAX_DELAY);
      if (ret == pdPASS) {
        // The message was successfully received - send it back to Serial port and "Echo: "
        Serial.printf("Echo button : %d\n", message.buttonClicked);
        // The item is queued by copy, not by reference, so lets free the buffer after use.
        switch (message.buttonClicked) {
          case 0:
            amplifier.playSDMusic(musicList[1].c_str());
            break;
          case 1:
            amplifier.playSDMusic(musicList[2].c_str());
            break;
          case 2:
            amplifier.playSDMusic(musicList[5].c_str());
            break;
          case 3:
            amplifier.playSDMusic(musicList[14].c_str());
            break;
          default:
            amplifier.playSDMusic(musicList[message.buttonClicked].c_str());
            break;
        }
        Serial.printf("End for button : %d\n", message.buttonClicked);
      } else if (ret == pdFALSE) {
        Serial.println("The message was unable to receive data from the Queue");
      }
    }
  }
}

void printMusicList(void) {
  uint8_t i = 0;

  if (musicList[i].length()) {
    Serial.println("\nMusic List: ");
  } else {
    Serial.println("The SD card audio file scan is empty, please check whether there are audio files in the SD card that meet the format!");
  }
  while (musicList[i].length() > 0) {
    Serial.print("\t");
    Serial.print(i);
    Serial.print("  -  ");
    Serial.println(musicList[i]);
    i++;
  }
  Serial.print("Found : ");
  Serial.println(i);
}


void parseSerialCommand(void) {
  String cmd;   // Save the command type read in the serial port
  float value;  // Save the command value read in the serial port

  /**
   * Command format: cmd-value
   * cmd : indicate the command type
   * value : indicate the set value corresponding to the command type, some commands can be empty
   * For example: (1) set high-pass filter, filter the audio data below 500: hp-500
   *      (2) close filter: closeFilter-
   */
  if (Serial.available()) {             // Detect whether there is an available serial command
    cmd = Serial.readStringUntil('-');  // Read the specified terminator character string, used to cut and identify the serial command.  The same comment won't repeat later.

    if (cmd.equals("hp")) {  // Determine if it’s the command type for setting high-pass filter
      Serial.println("Setting a High-Pass filter...\n");
      value = Serial.parseFloat();  // Parse character string and return floating point number

      /**
       * @brief Open audio filter
       * @param type - bq_type_highpass: open high-pass filtering; bq_type_lowpass: open low-pass filtering
       * @param fc - Threshold of filtering, range: 2-20000
       * @note For example, setting high-pass filter mode and the threshold of 500 indicates to filter out the audio signal below 500; high-pass filter and low-pass filter can work simultaneously.
       */
      amplifier.openFilter(bq_type_highpass, value);


    } else if (cmd.equals("lp")) {  // Determine if it's the command type for setting low-pass filter
      Serial.println("Setting a Low-Pass filter...\n");
      value = Serial.parseFloat();

      amplifier.openFilter(bq_type_lowpass, value);

    } else if (cmd.equals("closeFilter")) {  // Determine if it's the command type for closing filter
      Serial.println("Closing filter...\n");

      /**
       * @brief Close the audio filter
       */
      amplifier.closeFilter();

    } else if (cmd.equals("vol")) {  // Determine if it's the command type for setting volume
      Serial.println("Setting volume...\n");
      value = Serial.parseFloat();

      /**
       * @brief Set volume
       * @param vol - Set volume, the range can be set to 0-9
       * @note 5 for the original volume of audio data, no increase or decrease
       */
      amplifier.setVolume(value);

    } else if (cmd.equals("start")) {  // Determine if it's the command type for starting playback
      Serial.println("starting amplifier...\n");

      /**
       * @brief SD card music playback control interface
       * @param CMD - Playback control command: 
       * @n SD_AMPLIFIER_PLAY: Start to play music, which can be played from the position where you paused before
       * @n   If no music file is selected through playSDMusic(), the first one in the list will be played by default.
       * @n   Playback error may occur if music files are not scanned from SD card in the correct format (only support English for path name of music files and WAV for their format currently)
       * @n SD_AMPLIFIER_PAUSE: pause playback, keep the playback position of the current music file
       * @n SD_AMPLIFIER_STOP: stop playback, end the current music playback
       * @return None
       */
      amplifier.SDPlayerControl(SD_AMPLIFIER_PLAY);

    } else if (cmd.equals("pause")) {  // Determine if it's the command type for pausing playback
      Serial.println("Pause amplifier...\n");

      // The same as above
      amplifier.SDPlayerControl(SD_AMPLIFIER_PAUSE);

    } else if (cmd.equals("stop")) {  // Determine if it's the command type for stopping playback
      Serial.println("Stopping amplifier...\n");

      // The same as above
      amplifier.SDPlayerControl(SD_AMPLIFIER_STOP);

    } else if (cmd.equals("musicList")) {  // Determine if it's the command type for printing the list of the music files that can be played currently
      Serial.println("Scanning music list...\n");

      /**
       * @brief Scan the music files in WAV format in the SD card
       * @param musicList - The music files in .wav format scanned from the SD card. Type is character string array
       * @return None
       * @note Only support English for music file and path name currently and try to avoid spaces, only support .wav for the audio format currently
       */
      amplifier.scanSDMusic(musicList);
      /**
       * Print the list of the scanned music files that can be played
       */
      printMusicList();

    } else if (cmd.equals("changeMusic")) {  // Determine if it's the command type for changing songs according to the music list
      cmd = musicList[Serial.parseInt()];

      /**
       * @brief Play music files in the SD card
       * @param Filename - music file name, only support the music files in .wav format currently
       * @note Music file name must be an absolute path like /musicDir/music.wav
       * @return None
       */
      if (cmd.length()) {
        Serial.println("Changing Music...\n");
        amplifier.playSDMusic(cmd.c_str());
      } else {
        Serial.println("The currently selected music file is incorrect!\n");
      }

    } else {                                                                          // Unknown command type
      Serial.println("Help : \n \
      Currently available commands (format: cmd-value):\n \
        Start playback: e.g. start-\n \
        Pause playback: e.g. pause-\n \
        Stop playback: e.g. stop-\n \
        Print music list: e.g. musicList-\n \
        Change songs according to the music list: e.g. changeMusic-1\n \
        Set and open high-pass filter: e.g. hp-500\n \
        Set and open low-pass filter: e.g. lp-15000\n \
        Close filter: e.g. closeFilter-\n \
        Set volume: e.g. vol-5.0\n \
      For the detailed meaning, please refer to the code comments of this demo.\n");  //
    }
    while (Serial.read() >= 0)
      ;  // Clear the remaining data in the serial port
  }
}
