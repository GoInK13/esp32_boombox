# Boombox with CAN

## Hardware :
- Wemos / Nano / ESP32 ?
- MX-RM-5V (Receptor) : https://karooza.net/using-fixed-code-remotes-with-arduino

## Compilation
First you need to add the zip library in the libs folder to your arduino IDE :
Click on Sketch -> Include Library -> Add .ZIP library
Add the library : DFRobot\_MAX98357A from Sketch -> Include Library -> Manage Library

## Connection :  

### MC-RM-5V  
VCC : 5V
TX  : ESP\_22

### Transceiver CAN
TX  : ESP\_17
RX  : ESP\_16

### SD reader
SPI\_CS   : ESP\_5
SPI\_MOSI : ESP\_23
SPI\_MISO : ESP\_19
SPI\_CLK  : ESP\_18

### Ampli
BCLK      : ESP\_25
LRC       : ESP\_26
DIN       : ESP\_27


